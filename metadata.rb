name             'app-mariadb'
maintainer       'Videx Inc'
maintainer_email 'imanishi <imanishi@videx.co.jp>'
license          'All rights reserved'
description      'Installs/Configures app-mariadb'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
