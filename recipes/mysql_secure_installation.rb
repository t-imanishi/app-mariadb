### パスワードなしでmysqlに接続できてしまう場合は初期状態と見なし、以下を実行
bash 'mysql_secure_install emulate' do
  code <<-"EOH"
    /usr/bin/mysqladmin drop test -f
    /usr/bin/mysql -e "delete from user where user = '';" -D mysql
    /usr/bin/mysql -e "delete from user where user = 'root' and host = \'#{node[:fqdn]}\';" -D mysql
    /usr/bin/mysql -e "SET PASSWORD FOR 'root'@'::1' = PASSWORD(\'#{node['app-mariadb']['password']}\');" -D mysql
    /usr/bin/mysql -e "SET PASSWORD FOR 'root'@'127.0.0.1' = PASSWORD(\'#{node['app-mariadb']['password']}\');" -D mysql
    /usr/bin/mysql -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD(\'#{node['app-mariadb']['password']}\');" -D mysql
    /usr/bin/mysqladmin flush-privileges -p\'#{node['app-mariadb']['password']}\'
  EOH
  action :run
  only_if "/usr/bin/mysql -u root -e 'show databases;'"
end
