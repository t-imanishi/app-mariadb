#
# Cookbook Name:: app-mariadb
# Recipe:: default
#
# Copyright 2015, Videx Inc
#
# All rights reserved - Do Not Redistribute
#

### MariaDB 公式yumリポジトリを取得
cookbook_file '/etc/yum.repos.d/MariaDB.repo' do
  source "MariaDB.repo"
  owner "root"
  group "root"
  mode 0644
  action :create
end

### 既存 mysql(mariadb5.5)の削除
service "mysql" do
  action [:disable, :stop]
end

%w[ mariadb mariadb-server mariadb-libs ].each do | pkg |
  package pkg do
    action :remove
  end
end

### MariaDBインストール 有効化
%w[ MariaDB-client MariaDB-server ].each do | pkg |
  package pkg do
    action :install
  end
end

service "mariadb" do
  action [:enable, :start]
end

### データベース初期化
include_recipe "app-mariadb::mysql_secure_installation"

### cnfファイル投入
template "/etc/my.cnf.d/server.cnf" do
    source "server.cnf.erb"
    owner "root"
    group "root"
    notifies :restart, 'service[mariadb]'
end

### cnfファイル投入
template "/etc/my.cnf.d/mysql-clients.cnf" do
    source "mysql-clients.cnf.erb"
    owner "root"
    group "root"
    notifies :restart, 'service[mariadb]'
end
